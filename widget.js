var occam = {};

occam.getMetadata = function(index, key, callback) {
  var queuedEvent = function(event) {
    var data = event.data;

    if (data.name === 'getMetadata' && data.key === key && data.index === index) {
      callback(data);
      window.removeEventListener('message', queuedEvent);
    }
  };

  window.addEventListener('message', queuedEvent);

  window.parent.postMessage({
    'name':  'getMetadata',
    'index': index,
    'key':   key
  }, '*');
};

occam.setValue = function(index, key, value) {
  window.parent.postMessage({
    'name':  'setValue',
    'index': index,
    'key':   key,
    'value': value
  }, '*');
};

/* load dram device power consumption graph svg file into page */
$(function () {
  // Pull information out of the form
  $('.field').each(function() {
    var field = $(this);

    var configuration_id  = field.data('configuration');
    var configuration_key = field.data('key');

    var info = occam.getMetadata(configuration_id, configuration_key, function(data) {
      console.log("Updating " + configuration_key);
      console.log(data);
      var value = field.find('.value');
      var units = field.find('.units');
      console.log(value);

      var newValue = data.info.value;
      console.log(newValue);

      if (configuration_key == "scheduling_policy") {
        value.text(newValue.split('_').slice(0,3).join(' '));
      }
      else {
        value.text(newValue);
      }
    });
  });

  var dram_node = $('li.dram');

  var updateChannels = function() {
    var num_chans = $('li.dram').length;

    var ganged = true;
    if (ganged) {
      var newValue = 64 * num_chans;
      occam.setValue("0", "num_chans", "1");
      occam.setValue("0", "jedec_data_bus_bits", newValue);
    }
    else {
      var newValue = num_chans;
      occam.setValue("0", "num_chans", newValue);
      occam.setValue("0", "jedec_data_bus_bits", "64");
    }
  };

  $('.add-dram').on('click', function(event) {
    var new_dram_node = dram_node.clone();
    new_dram_node.children('h2').text('Channel ' + ($('li.dram').length + 1));
    $('ul.dram-nodes').append(new_dram_node);

    // Show remove dram button
    $('.remove-dram').css({"display": "block"});

    // Update num_chans or jedec_data_bus_bits
    updateChannels();

    event.stopPropagation();
    event.preventDefault();
  });

  $('.remove-dram').on('click', function(event) {
    $('li.dram').last().remove();
    if ($('li.dram').length == 1) {
      $(this).css({"display": "none"});
    }

    updateChannels();

    event.stopPropagation();
    event.preventDefault();
  });

  // Pull initial channel info
  var loaded = 0;
  var loadChannelInfo = function() {
    var num_init_channels = num_chans_info.value;

    if (num_chans_info.value == "1") {
      num_init_channels = parseInt(bus_size_info.value) / 64;
    }

    for (var i = 1; i < num_init_channels; i++) {
      $('.add-dram').trigger('click');
    }
  };

  var num_chans_info = {};
  occam.getMetadata("0", "num_chans", function(data) {
    num_chans_info = data.info;
    loaded++;
    if (loaded == 2) {
      loadChannelInfo();
    }
  });
  var bus_size_info = {};
  occam.getMetadata("0", "jedec_data_bus_bits", function(data) {
    bus_size_info = data.info;
    loaded++;
    if (loaded == 2) {
      loadChannelInfo();
    }
  });

  var updateField = function(element, difference) {
    var field = element.parents('.field').first();
    var value = field.find('.value');
    var units = field.find('.units');

    var configuration_id  = field.data('configuration');
    var configuration_key = field.data('key');

    occam.getMetadata(configuration_id, configuration_key, function(data) {
      var info = data.info;
      var newValue = value.text();

      if (info.type == "enum") {
        var index = value.data('index');

        if (index < 0) {
          index = 0;
        }
        else {
          index += difference;
          if (index < 0) {
            index = info['options'].length-1;
          }
          else {
            index %= info['options'].length;
          }
        }

        newValue = info['options'][index];

        value.data('index', index);
      }
      else {
        newValue = parseInt(value.text()) + difference;
      }

      if (configuration_key == "scheduling_policy") {
        value.text(newValue.split('_').slice(0,3).join(' '));
      }
      else {
        value.text(newValue);
      }

      occam.setValue(configuration_id, configuration_key, newValue);
    });
  };

  $('.down').on('click', function(event) {
    updateField($(this), -1);

    event.stopPropagation();
    event.preventDefault();
  });

  $('.up').on('click', function(event) {
    updateField($(this), 1);

    event.stopPropagation();
    event.preventDefault();
  });

  $('.value').on('click', function(event) {
    // Turn it into a text field
    var value_field = $(this);
    var text_field = $('<input type="text" class="value"></input>');
    text_field.val(value_field.text());
    text_field.on('blur', function(event) {
      var newValue = $(this).val();
      value_field.text(newValue);
      $(this).remove();
      value_field.show();

      updateField(value_field, 0);
    });
    $(this).hide();
    $(this).after(text_field);
    text_field.focus();
  });
});
